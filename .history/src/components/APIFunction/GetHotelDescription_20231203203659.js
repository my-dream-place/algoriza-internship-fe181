import { useAPIData } from "@/stores/APIDataStore";
import axios from "axios";

const dataStore = useAPIData();

const getHotelData = async (hotelID) => {
  const options = {
    method: "GET",
    url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/getDescriptionAndInfo",
    params: {
      hotel_id: `${hotelID}`,
    },
    headers: {
      "X-RapidAPI-Key": "26e864ba6bmsh1d64b18aab28e2bp10ec8bjsn712a765323dc",
      "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
    },
  };

  try {
    const response = await axios.request(options);
    dataStore.hotelDescription = response.data.data;
    console.log(dataStore.hotelDescription);
  } catch (error) {
    // inputsAreInvalid.value = true;
    console.error(error.message);
  }
  return;
};

export default getHotelData;
