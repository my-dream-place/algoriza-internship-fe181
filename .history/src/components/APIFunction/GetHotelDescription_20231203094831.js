import { useAPIData } from "@/stores/APIDataStore";
import axios from "axios";

const dataStore = useAPIData();

const getHotelData = async (hotelID) => {
  const options = {
    method: "GET",
    url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/getDescriptionAndInfo",
    params: {
      hotel_id: `${hotelID}`,
    },
    headers: {
      "X-RapidAPI-Key": "e5ea8a7034msh355f0881f507141p19fb35jsn720bf5e31651",
      "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
    },
  };

  try {
    const response = await axios.request(options);
    dataStore.hotelDescription = response.data.data;
    console.log(dataStore.hotelDescription);
  } catch (error) {
    // inputsAreInvalid.value = true;
    console.error(error.message);
  }
  return;
};

export default getHotelData;
