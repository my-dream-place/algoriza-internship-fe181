import { useAPIData } from "@/stores/APIDataStore";
import { keepDataAlive } from "@/stores/KeepingDataAlive";
import axios from "axios";

const keepDataAliveStore = keepDataAlive();

const dataStore = useAPIData();

const getAPIFilteredData = async (min, max, sortID, pageNum) => {
  // if (searchQuery.value === "" || indate.value === "" || outdate.value === "") {
  //   inputsAreInvalid.value = true;
  //   console.log(checkinDate.value);
  //   return;
  // }

  const options = {
    method: "GET",
    url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels",
    params: {
      dest_id: `${keepDataAliveStore.cityID}`,
      search_type: "city",
      arrival_date: `${keepDataAliveStore.checkinDate}`,
      departure_date: `${keepDataAliveStore.checkoutDate}`,
      adults: `${keepDataAliveStore.guestNumber || 1}`,
      children_age: "0,17",
      room_qty: `${keepDataAliveStore.roomsNumber || 1}`,
      page_number: `${pageNum || "1"}`,
      languagecode: "en-us",
      currency_code: "USD",
      price_min: `${min || ""}`,
      price_max: `${max || ""}`,
      sort_by: `${sortID || ""}`,
    },
    headers: {
      "X-RapidAPI-Key":
        "98e61d9e08msh16d2084b0088c49p156564jsnd68c3f0bef25      ",
      "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
    },
  };

  try {
    const response = await axios.request(options);
    console.log(response.data);
    if (response.data.data.hotels) {
      dataStore.hotelSearchData = response.data.data.hotels;
      dataStore.realData = response.data.data.hotels;
      dataStore.metaData = response.data.data.meta;
    }
  } catch (error) {
    // inputsAreInvalid.value = true;
    console.error(error.message);
  }
  return;
};

export default getAPIFilteredData;
