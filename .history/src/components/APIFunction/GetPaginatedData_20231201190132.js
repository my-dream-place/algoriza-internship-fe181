import { useAPIData } from "@/stores/APIDataStore";
import { keepDataAlive } from "@/stores/KeepingDataAlive";
import axios from "axios";

const keepDataAliveStore = keepDataAlive();

const dataStore = useAPIData();

const getAPIPaginatedData = async (pageNum) => {
  const options = {
    method: "GET",
    url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/searchHotels",
    params: {
      dest_id: `${keepDataAliveStore.cityID}`,
      search_type: "city",
      arrival_date: `${keepDataAliveStore.checkinDate}`,
      departure_date: `${keepDataAliveStore.checkoutDate}`,
      adults: `${keepDataAliveStore.guestNumber || 1}`,
      children_age: "0,17",
      room_qty: `${keepDataAliveStore.roomsNumber || 1}`,
      page_number: `${pageNum || "1"}`,
      languagecode: "en-us",
      currency_code: "USD",
    },
    headers: {
      "X-RapidAPI-Key": "2198c29ee2msh29ba2dc07db703bp159e78jsnd88863ce5ed5",
      "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
    },
  };

  try {
    const response = await axios.request(options);
    console.log(response.data);
    dataStore.paginationResult = response.data.data.hotels;
    dataStore.realData = response.data.data.hotels;
    dataStore.realDataForBudget = response.data.data.hotels;
    console.log(dataStore.paginationResult);
  } catch (error) {
    // inputsAreInvalid.value = true;
    console.error(error.message);
  }
  return;
};

export default getAPIPaginatedData;
