import { useAPIData } from "@/stores/APIDataStore";
import axios from "axios";

const dataStore = useAPIData();

const getHotelData = async (hotelID) => {
  const options = {
    method: "GET",
    url: "https://booking-com15.p.rapidapi.com/api/v1/hotels/getDescriptionAndInfo",
    params: {
      hotel_id: `${hotelID}`,
    },
    headers: {
      "X-RapidAPI-Key": "2198c29ee2msh29ba2dc07db703bp159e78jsnd88863ce5ed5",
      "X-RapidAPI-Host": "booking-com15.p.rapidapi.com",
    },
  };

  try {
    const response = await axios.request(options);
    dataStore.hotelDescription = response.data.data;
    console.log(dataStore.hotelDescription);
  } catch (error) {
    // inputsAreInvalid.value = true;
    console.error(error.message);
  }
  return;
};

export default getHotelData;
